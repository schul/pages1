function createNode(element) {
    return document.createElement(element);
}

function append(parent, el) {
    return parent.appendChild(el);
}

function htmlchars(html) {
    if (html != null) {
        return html.replace(/</g, "&lt;").replace(/>/g, "&gt;")
    };
    return
}

const root = document.getElementById('elements');
const url = 'elements.json';

fetch(url)
    .then((resp) => resp.json())
    .then(function (data) {
        return data.map(function (element) {
            let div = createNode('tr');
            div.innerHTML = `
                <th scope='row'><code>${htmlchars(element.html)}</code></th>
                <td>${htmlchars(element.desc)}</td>
                <td><button type='button' class='btn btn-primary' data-bs-toggle='modal' data-bs-target='#${element.id}'>Example</button></td>\
                
                <div class='modal fade' id="${element.id}" tabindex="-1" aria-labelledby="${element.id}Label" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="${element.id}Label"><code>${htmlchars(element.html)}</code></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="code">
                                    <span class="lang">html</span>
                                    <pre>${htmlchars(element.example)}</pre>
                                </div>
                                ${element.example}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            `
            append(root, div);
        })
    })
    .catch(function (error) {
        console.log(error);
    }
);